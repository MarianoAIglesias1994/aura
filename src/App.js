import React from "react";
import { Box, TextField, MenuItem, Card, CardContent } from "@mui/material";

const useCases = [
  {
    value: "asesor",
    label: "Asesor/Productor",
  },
  {
    value: "arrendamiento_campo",
    label: "Arrendamiento de campos",
  },
  {
    value: "aseguradoras",
    label: "Aseguradoras",
  },
  {
    value: "compra_granos",
    label: "Compra de granos",
  },
];

export default function App() {
  const [useCase, setUseCase] = React.useState();

  const handleChange = (event) => {
    setUseCase(event.target.value);
  };

  return (
    <Card sx={{ maxWidth: 345, margin: "auto", marginTop: "15px"}}>
      <CardContent>
        <Box
          component="form"
          sx={{ "& .MuiTextField-root": { m: 5, width: "80%"}}}
          noValidate
          autoComplete="off"
        >
          <div>
            <TextField
              id="demo-simple-select"
              className="select"
              select
              label="Select"
              value={useCase}
              onChange={handleChange}
              helperText="Por favor seleccione un caso de uso"
            >
              {useCases.map((option) => (
                <MenuItem key={option.value} value={option.value}>
                  {option.label}
                </MenuItem>
              ))}
            </TextField>
          </div>
        </Box>
      </CardContent>
    </Card>
  );
}
