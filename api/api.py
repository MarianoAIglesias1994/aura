from flask import Flask, jsonify, request
#from best_practices import BP
#from productivity import Productivity
#from sustentability import Sustentability
from farm import *
from field import *
import json
app = Flask(__name__)

@app.route("/")
def hello_world():
    return "<p>AuravantHack Team 13 Api :) </p>"
@app.route("/api/farm", methods=['GET', 'POST'])
def farm():
    data = request.data
    data = json.loads(data)
    farm = Farm(data['farm_id'])
    farm.get_data()
    response = app.response_class(
        response=json.dumps(farm.data),
        status=200,
        mimetype='application/json'
    )
    return response
@app.route("/api/field", methods=['GET', 'POST'])
def field():
    data = request.data
    data = json.loads(data)
    farm = Farm(data['farm_id'])
    farm.get_data()
    field = Field(data['field_id'],farm.data['fields'][data['field_id']])
    response = app.response_class(
        response=json.dumps(field.data),
        status=200,
        mimetype='application/json'
    )
    return response
@app.route("/api/field/ndvi", methods=['GET', 'POST'])
def field_nvdi():
    data = request.data
    data = json.loads(data)
    farm = Farm(data['farm_id'])
    farm.get_data()
    field = Field(data['field_id'],farm.data['fields'][data['field_id']])
    response = app.response_class(
        response=json.dumps(field.get_ndvi_mean(data['date_from'],data['date_to'])),
        status=200,
        mimetype='application/json'
    )
    return response
@app.route("/api/farm/data", methods=['GET', 'POST'])
def farm_data():
    data = request.data
    data = json.loads(data)
    farm = Farm(data['farm_id'])
    farm.get_data()
    farm_output={
    "farm":{
        "farm_id": farm.farm_id,
        "fields": []
        }
    }
    for field_id in farm.fields:
        field = Field(field_id,farm.data['fields'][field_id])
        field.get_factors()
        field.get_indicators()
        field.get_value()
        output = field.get_output()
        farm_output["farm"]["fields"].append(output)
    response = app.response_class(
        response=json.dumps(farm_output),
        status=200,
        mimetype='application/json'
    )
    return response

# Checks to see if the name of the package is the run as the main package.
if __name__ == "__main__":
    # Runs the Flask application only if the main.py file is being run.
    app.run()