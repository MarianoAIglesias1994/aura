import json
import requests
class Farm:
  def __init__(self, farm_id):
    self.token = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzcmMiOiJ3IiwidWlkIjoiVUlELWQ0NmIyYjI2NjQ0NjViNWIyMjBlMDU3MWYwOTY3MmQ1IiwiZXhwIjoxNjQwNjIyOTg5LCJ2Ijo2OTMsImxvY2FsZSI6ImVzX0FSIiwiZGV2Ijo5Nn0.V9_g-HngfgCGmDl_eXNw_GxXEWSU1gfNWNyXqIWB-Eg"
    self.farm_id = str(farm_id)
    self.fields = []
    self.api_url = "https://api.auravant.com/api/getfields"
    self.req_data = {"user":{"farms": {"farm_id": self.farm_id }}}
    self.data = None
    self.fields = []
  def get_data(self):
    headers = {'Authorization': 'Bearear ' + self.token}
    url = self.api_url
    data = self.req_data
    response = requests.get(url, headers=headers, data=data).json()
    self.data = response["user"]["farms"][self.farm_id]
    self.fields = self.data["fields"].keys()
