import json
import requests
import random
from statistics import mean
class Field:
  def __init__(self, field_id, field_data):
    self.token = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzcmMiOiJ3IiwidWlkIjoiVUlELWQ0NmIyYjI2NjQ0NjViNWIyMjBlMDU3MWYwOTY3MmQ1IiwiZXhwIjoxNjQwNjIyOTg5LCJ2Ijo2OTMsImxvY2FsZSI6ImVzX0FSIiwiZGV2Ijo5Nn0.V9_g-HngfgCGmDl_eXNw_GxXEWSU1gfNWNyXqIWB-Eg"
    self.field_id = field_id
    self.value = None
    self.value_default_weights = [0.34, 0.33, 0.33]
    self.data = field_data
    self.coordinates = field_data["shapes"]["current"]["polygon"]
    self.risks = {
      "factors":{
        "probabilidad_inundacion": None, #0.4
        "probabilidad_granizo": None, #0.2
        "probabilidad_sequia": None #0.4
      },
      "weights": [0.4, 0.2, 0.4],
      "indicator": None
    }
    self.sustentability = {
      "factors":{
        "sin_deforestacion": False, #0.6
        "rotacion_de_cultivos": False,#0.3
        "bpa": False #0.1
      },
      "weights": [0.6, 0.3, 0.1],
      "indicator": None
    }
    self.productivity = {
      "factors":{
        "productividad_gruesa": None, #0.6
        "productividad_suelos_inta": None #0.4
      }  ,
      "weights": [0.6, 0.4],
      "indicator": None      
    }
  def get_factors(self):
    #Riesgos
    self.risks["factors"]["probabilidad_inundacion"] = self.get_probabilidad_inundacion()
    self.risks["factors"]["probabilidad_granizo"] = self.get_probabilidad_granizo()
    self.risks["factors"]["probabilidad_sequia"] = self.get_probabilidad_sequia()
    #Sustentabilidad
    self.sustentability["factors"]["sin_deforestacion"] = self.get_sin_deforestacion()
    self.sustentability["factors"]["rotacion_de_cultivos"] = self.get_rotacion_de_cultivos()
    self.sustentability["factors"]["bpa"] = self.get_bpa()
    #Productividad
    self.productivity["factors"]["productividad_gruesa"] = self.get_productividad_gruesa()
    self.productivity["factors"]["productividad_suelos_inta"] = self.get_productividad_suelos_inta()
  def get_indicators(self):
    #Riesgos
    self.risks["indicator"] = round(self.risks["weights"][0]*self.risks["factors"]["probabilidad_inundacion"] + self.risks["weights"][1]*self.risks["factors"]["probabilidad_granizo"] + self.risks["weights"][2]*self.risks["factors"]["probabilidad_sequia"],3)
    #Sustentabilidad
    self.sustentability["indicator"] = round(self.sustentability["weights"][0]*self.sustentability["factors"]["sin_deforestacion"]  + self.sustentability["weights"][1]*self.sustentability["factors"]["rotacion_de_cultivos"] + self.sustentability["weights"][2]*self.sustentability["factors"]["bpa"],3)
    #Productividad
    self.productivity["indicator"] = round(self.productivity["weights"][0]*self.productivity["factors"]["productividad_gruesa"] + self.productivity["weights"][1]*self.productivity["factors"]["productividad_suelos_inta"],3)
  def get_value(self):
    #Valor
    self.value = round(self.value_default_weights[0]*self.risks["indicator"] + self.value_default_weights[1]*self.sustentability["indicator"]  + self.value_default_weights[2]*self.productivity["indicator"] ,3)
  def get_ndvi_mean(self, date_from, date_to):
    headers = {'Authorization': 'Bearear ' + self.token}
    url = "https://api.auravant.com/api/fields/ndvi"
    params = {"field_id": self.field_id, "date_from": date_from, "date_to": date_to }
    response = requests.get(url, headers=headers, params=params).json()
    ndvi = []
    for point in response["ndvi"]:
      ndvi.append(point["ndvi_mean"])
    return mean(ndvi)
  def get_probabilidad_inundacion(self):
    result = round(random.random(),3)
    return result
  def get_probabilidad_granizo(self):
    result = round(random.random(),3)
    return result
  def get_probabilidad_sequia(self):
    result = round(random.random(),3)
    return result
  def get_sin_deforestacion(self):
    result = random.choice([0, 1])
    return result
  def get_rotacion_de_cultivos(self):
    result = random.choice([0, 1])
    return result
  def get_bpa(self):
    result = random.choice([0, 1])
    return result
  def get_productividad_gruesa(self):
    anios = [2015,2016,2017,2018,2019,2020]
    ndvis = []
    for anio in anios:
      ndvi = self.get_ndvi_mean(str(anio) + "-12-01", str(anio + 1) +"-04-01")
      ndvis.append(ndvi)
    result = round(mean(ndvis),3)
    return result
  def get_productividad_suelos_inta(self):
    result = round(random.random(),3)
    return result
  def get_output(self):
    output = dict(self.__dict__)
    output.pop("coordinates")
    output.pop("token")
    output.pop("value_default_weights")
    output.pop("data")
    return output